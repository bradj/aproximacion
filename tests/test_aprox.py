#!/usr/bin/python3
# -*- coding: utf-8 -*-

import contextlib
from io import StringIO
import os
import sys
import unittest

from unittest.mock import patch

import aprox

this_dir = os.path.dirname(os.path.abspath(__file__))
parent_dir = os.path.join(this_dir, '..')

class TestComputeTrees(unittest.TestCase):

    def test_simple(self):
        aprox.base_trees = 25
        aprox.fruit_per_tree = 400
        aprox.reduction = 10
        result = aprox.compute_trees(28)
        self.assertEqual(10360, result)

class TestComputeAll(unittest.TestCase):

    def test_simple(self):
        aprox.base_trees = 25
        aprox.fruit_per_tree = 400
        aprox.reduction = 10
        result = aprox.compute_all(28, 33)
        self.assertEqual([(28, 10360), (29, 10440), (30, 10500), (31, 10540), (32, 10560), (33, 10560)],
                         result)

    def test_simple2(self):
        aprox.base_trees = 35
        aprox.fruit_per_tree = 600
        aprox.reduction = 15
        result = aprox.compute_all(28, 31)
        self.assertEqual([(28, 19740), (29, 20010), (30, 20250), (31, 20460)],
                         result)

class TestReadArguments(unittest.TestCase):

    def test_simple(self):
        with patch.object(sys, 'argv', ['aprox.py', '25', '400', '10', '25', '45']):
            args = aprox.read_arguments()
        self.assertEqual((25, 400, 10, 25, 45), args)

    def test_simple2(self):
        with patch.object(sys, 'argv', ['aprox.py', '50', '800', '20', '50', '90']):
            args = aprox.read_arguments()
        self.assertEqual((50, 800, 20, 50, 90), args)

    def test_integers(self):
        with self.assertRaises(SystemExit) as context:
            with patch.object(sys, 'argv', ['aprox.py', '50', '800', '20', 'A', '90']):
                args = aprox.read_arguments()
        self.assertTrue("All arguments must be integers", context.exception.code)

    def test_integers2(self):
        with self.assertRaises(SystemExit) as context:
            with patch.object(sys, 'argv', ['aprox.py', 'A', '800', '20', '50', '90']):
                args = aprox.read_arguments()
        self.assertTrue("All arguments must be integers", context.exception.code)

    def test_number(self):
        with patch.object(sys, 'argv', ['aprox.py', '50', '800', '20', '50']):
            with self.assertRaises(SystemExit) as context:
                args = aprox.read_arguments()
        self.assertTrue(f"Usage: {sys.argv[0]} <base_trees> <fruit_per_tree> <reduction> <min> <max>",
                        context.exception.code)


expected = """27 10260
28 10360
29 10440
30 10500
31 10540
32 10560
33 10560
34 10540
Best production: 10560, for 32 trees
"""

class TestOutput(unittest.TestCase):

    def test_output(self):

        stdout = StringIO()
        with patch.object(sys, 'argv', ['aprox.py', '25', '400', '10', '27', '34']):
            with contextlib.redirect_stdout(stdout):
                aprox.main()
        output = stdout.getvalue()
        self.assertEqual(expected, output)

if __name__ == '__main__':
    unittest.main(module=__name__, buffer=True, exit=False)
